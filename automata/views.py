from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from .pila import Pila
from .estado import Estado
from .transicion import Transicion
import json

# Create your views here.
def index(request):
    lista = {"q0", "q1", "q2"}
    context = {'lista': lista}
    cargarAutomata()
    return render(request, 'automata/index.html', context)

@csrf_exempt
def estados(request):
    hola = {"que", "pasa", "xd"}
    context = {'dato': hola}
    if request.method == 'POST':
        return render(request, "automata/index.html", context)
    else:
        return HttpResponse('404')

def cargarAutomata():
    estados = []
    cadena = 'abbaabal'
    print((len(cadena)-1)%2)
    if (len(cadena)-1)%2 == 0:
      with open("par-datos.json") as e:
          e = json.load(e)
      print("entro en par")
    else:
      with open("impar-datos.json") as e:
          e = json.load(e)
      print("entro en impares")
    for estado in e['estados']:
        estados.append(Estado(estado))   

    for transicion in e['transiciones']:
        auxEstadoInicial = None
        auxEstuadoFinal = None
        caracter = None
        buscar = None
        insertar = None
        for estado in estados:
            if estado.getEstado() == 'q2':
                aceptacion = estado
            if transicion['inicio'] == estado.getEstado():
                auxEstadoInicial = estado
                caracter = transicion['cadena']
                buscar = transicion['buscar']
                insertar = transicion['insertar']

            if transicion['fin'] == estado.getEstado():
                auxEstuadoFinal = estado

        t = Transicion(auxEstadoInicial, auxEstuadoFinal, caracter, buscar, insertar)
        auxEstadoInicial.setTransicion(t)

    p = Pila('#')
    actual = estados[0]


    for caracter in range(len(cadena)):
            for estado in estados:
                for x in estado.getTransiciones():
                    if cadena[caracter] == x.getCaracter() and x.getBuscar() == p.getTope() and estado.getEstado() == actual.getEstado():
                        p.removeDate()
                        print(actual.getEstado())
                        for insert in x.getInsertar():
                            if insert != 'l':
                                p.pushDate(insert)
                        p.viewPila()
                    actual = x.getFin()

    if actual.getEstado() == aceptacion.getEstado():
            print("La palabra [ "+ cadena[:len(cadena)-1] +" ] fue aceptada")
    else:
            print("La palabra [ "+ cadena[:len(cadena)-1] +" ] no fue aceptada")

